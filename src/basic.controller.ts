import {Command, CommandRunner, Option} from 'nest-commander';

interface BasicCommandOptions {
  string?: string;
  boolean?: boolean;
  number?: number;
}

@Command({
  name: 'basic',
  description: 'A parameter parse'
})
export class BasicCommand extends CommandRunner {
  async run(
    passedParam: string[],
    options?: BasicCommandOptions,
  ): Promise<void> {
    if (options?.number) {
      this.runWithNumber(passedParam, options.number);
    } else if (options?.string) {
      this.runWithString(passedParam, options.string);
    } else {
      this.runWithNone(passedParam);
    }
  }

  @Option({
    flags: '-n, --number [number]',
    description: 'A basic number parser',
  })
  parseNumber(val: string): number {
    console.log(`parse numb`, val)
    return Number(val);
  }

  @Option({
    flags: '-s, --string [string]',
    description: 'A string return',
  })
  parseString(val: string): string {
    console.log(`parse str`, val)
    return val;
  }

  @Option({
    flags: '-b, --boolean [boolean]',
    description: 'A boolean parser',
  })
  parseBoolean(val: string): boolean {
    console.log(`parse bool`, val)
    return JSON.parse(val);
  }

  runWithString(param: string[], option: string): void {
    console.log(`string`)
  }

  runWithNumber(param: string[], option: number): void {
    console.log(`num`)
  }

  runWithBoolean(param: string[], option: boolean): void {
    console.log(`bool`)
  }

  runWithNone(param: string[]): void {
    console.log(`none`)
  }
}
