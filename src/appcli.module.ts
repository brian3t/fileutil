import { Module } from '@nestjs/common';
// import {BasicCommand} from "./basic.controller";
import { RenameCommand } from './rename.controller';
import { ImgresizeCommand } from './imgresize.controller';

@Module({
  providers: [RenameCommand, ImgresizeCommand],
})
export class AppModule {}
