import { Command, CommandRunner, Option } from "nest-commander"
import * as process from "process"

const _ = require("lodash")

const Path = require("path")
const fs = require("fs")

interface RenameCommandOptions {
  path: string
  action: string
  startnum?: number
  boolean?: boolean
}

@Command({
  name: "rename",
  description: "Rename files in a folder"
})
export class RenameCommand extends CommandRunner {
  async run(
    passedParam: string[],
    options?: RenameCommandOptions
  ): Promise<void> {
    if (!options?.path) {
      console.error(`Error, need path -p`)
      return
    }
    if (!options?.action) {
      console.error(`Error, need action -a`)
      return
    }
    switch (options.action) {
      case "number": {
        this.runNumber(options)
        return
      }
    }
  }

  @Option({
    flags: "-p, --path [string]",
    description: "Path"
  })
  parsePath(val: string): string {
    console.log(`parse path`, val)
    return val
  }

  @Option({
    flags: "-a, --action [string]",
    description: "Action"
  })
  parseAction(val: string): string {
    console.log(`parse action`, val)
    return val
  }

  @Option({
    flags: "-b, --boolean [boolean]",
    description: "A boolean parser"
  })
  parseBoolean(val: string): boolean {
    console.log(`parse bool`, val)
    return JSON.parse(val)
  }

  @Option({
    flags: "-startnum, --start_num [integer]",
    description: "Start number"
  })
  parseInt(val: string): number {
    console.log(`parse start num`, parseInt(val))
    return parseInt(val)
  }

  runWithString(param: string[], option: string): void {
    console.log(`string`)
  }

  runWithNumber(param: string[], option: number): void {
    console.log(`num`)
  }

  runWithBoolean(param: string[], option: boolean): void {
    console.log(`bool`)
  }

  runWithNone(param: string[]): void {
    console.log(`none`)
  }

  /**
   * Number 01, 03, 05, etc...
   * Any file that's not a number becomes a number
   * @param params
   */
  runNumber(params) {
    try {
      const PAD_ZERO = 4 //pad zero up to x characters
      const path = params.path
      const start_num = params.start_num || 1
      const directory_abs_path = Path.join(__dirname, "inp", path)
      if (!fs.existsSync(directory_abs_path)) {
        console.error(`Error, path does not exist`)
        return
      }
      const out_dir_abs_path = __dirname + '/out//' + path + '/'
      if (!fs.existsSync(out_dir_abs_path)) fs.mkdirSync(out_dir_abs_path, {recursive: true})
      process.chdir(directory_abs_path)
      console.log(`Curr dir:`, process.cwd())
      fs.readdir('.', async function(err, files) {
        //handling error
        if (err) {
          return console.log("Unable to scan directory: " + err)
        }
        //listing all files using forEach
        const files_count = files.length
        let i = start_num, new_filename: string
        for (const file of files) {
          // Do whatever you want to do with the file
          const filename = file.toString()
          if (!(_.isNumber(filename))) {
            // console.log(`not number `, filename)
            new_filename = i.toString().padStart(4, "0")
            new_filename += Path.extname(filename)
            await fs.copyFileSync(filename, Path.join(out_dir_abs_path, new_filename))
            i = i + 2
          }
        }
      })
    }
    catch (e){
      console.error(`error:`, e)
    }
  }
}
