import { CommandFactory } from 'nest-commander';
import { AppModule } from './appcli.module';

async function bootstrap() {
  await CommandFactory.run(AppModule);
}
bootstrap();
