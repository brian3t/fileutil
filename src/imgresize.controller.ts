import { Command, CommandRunner, Option } from "nest-commander"
import * as process from "process"

const _ = require("lodash")
const { execSync } = require("child_process")

const Path = require("path")
const fs = require("fs")

interface ImgresizeCommandOptions {
  path?: string
  action?: string
  startnum?: number
  boolean?: boolean
}

@Command({
  name: "imgresize",
  description: "Resize image files in a folder. Default input folder: inp/."
})
export class ImgresizeCommand extends CommandRunner {
  async run(
    passedParam: string[],
    options?: ImgresizeCommandOptions
  ): Promise<void> {
    if (!options?.path) {
      options.path = "."
    }
    if (!options?.action) {
      options.action = "resize"
    }
    switch (options.action) {
      case "resize": {
        this.runResize(options)
        return
      }
    }
  }

  @Option({
    flags: "-p, --path [string]",
    description: "Path"
  })
  parsePath(val: string): string {
    console.log(`parse path`, val)
    return val
  }

  @Option({
    flags: "-a, --action [string]",
    description: "Action"
  })
  parseAction(val: string): string {
    console.log(`parse action`, val)
    return val
  }

  @Option({
    flags: "-b, --boolean [boolean]",
    description: "A boolean parser"
  })
  parseBoolean(val: string): boolean {
    console.log(`parse bool`, val)
    return JSON.parse(val)
  }

  @Option({
    flags: "-startnum, --start_num [integer]",
    description: "Start number"
  })
  parseInt(val: string): number {
    console.log(`parse start num`, parseInt(val))
    return parseInt(val)
  }

  runWithString(param: string[], option: string): void {
    console.log(`string`)
  }

  runWithNumber(param: string[], option: number): void {
    console.log(`num`)
  }

  runWithBoolean(param: string[], option: boolean): void {
    console.log(`bool`)
  }

  runWithNone(param: string[]): void {
    console.log(`none`)
  }

  /**
   * Resize each image in the path
   * @param params
   */
  runResize(params) {
    try {
      const path = params.path
      const directory_abs_path = Path.join(__dirname, "inp", path)
      if (!fs.existsSync(directory_abs_path)) {
        console.error(`Error, path does not exist`, directory_abs_path)
        return
      }
      const out_dir_abs_path = __dirname + "/out//" + path + "/"
      if (!fs.existsSync(out_dir_abs_path)) fs.mkdirSync(out_dir_abs_path, { recursive: true })
      process.chdir(directory_abs_path)
      // console.log(`Curr dir:`, process.cwd())
      fs.readdir(".", async function(err, files) {
        //handling error
        if (err) {
          return console.error("Unable to scan directory: " + err)
        }
        //listing all files using forEach
        const files_count = files.length
        let new_filename_sm: string, new_filename_md: string, cmd_str: string
        for (const file of files) {
          // Do whatever you want to do with the file
          const filename = file.toString()
          const fn_wo_ext = Path.parse(filename).name
          if (!_.isNumber(parseInt(fn_wo_ext)) || fn_wo_ext.includes("_")) {
            continue //bad file name; or already a resized file. We only need to process the root image
          }
          new_filename_sm = fn_wo_ext + "_60x60" + Path.extname(filename)
          cmd_str = `convert ${filename} -resize 60x60 ${new_filename_sm}`
          await execSync(cmd_str)
          // await fs.copyFileSync(filename, Path.join(out_dir_abs_path, new_filename_sm))
        }
      })
    } catch (e) {
      console.error(`error:`, e)
    }
  }
}
